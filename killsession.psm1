<#
  .Synopsis
   Terminates users RDP sessions on Servers

  .Description
   Parses all domain joined computers, walks thru that list (only matching *server*), either displaying user sessions on servers, or disconnecting them.

  .Example 
   # List all sessions on servers
   killsession

  .Example
   # Searching for tstJamie
   killsession -Identity tstJamie
   -or-
   killsession tstJamie

  .Example
   # Kill all disconnected sessions for tstJamie
   killsession -Identity tstJamie -Disconnect
   -or-
   killsession tstJamie -Disconnect

#>

Function killsession {
  param (
    [string[]]$Identitys = "nouser",
    [switch]$Disconnect = $false,
    [switch]$DiscoAll = $false
  )

  Import-Module ActiveDirectory
  $ServDate = (Get-Date).AddDays(-30)
  $Servers = Get-ADComputer -Properties * -Filter {(OperatingSystem -like "*server*") -and (LastLogonDate -gt $ServDate)}
  
  ForEach ($Server in $Servers) {
    $ServerName = $Server.Name
    Write-Output $("Querying $($ServerName)")
    try {
      $qResult = qwinsta /server:$ServerName
      $qProperties = ($qResult[0].Trim(" ") -replace("\b *\B")).Split(" ")
      $qSessions = $qResult[1..$($qResult.Count -1)]
      Foreach ($Session in $qSessions) {
        $hash = [ordered]@{
          $qProperties[0] = $Session.Substring(1,18).Trim()
          $qProperties[1] = $Session.Substring(19,22).Trim()
          $qProperties[2] = $Session.Substring(41,7).Trim()
          $qProperties[3] = $Session.Substring(48,8).Trim()
          $qProperties[4] = $Session.Substring(56,12).Trim()
          $qProperties[5] = $Session.Substring(68,8).Trim()
          'ComputerName' = "$ComputerName"
        }
        $qObj = New-Object -TypeName PSObject -Property $hash

        if($qObj.STATE -eq "Disc") {
          $qObj.STATE = "Disconnected"
        }

        if(($qObj.USERNAME -match "[a-z]") -and ($qObj.USERNAME -ne $NULL)) {
          if(($Identitys -contains $qObj.USERNAME) -or ($DiscoAll -ne $false)) {
            Write-Output $("$($ServerName): $($qObj.USERNAME) in state $($qObj.STATE)")
          }
        }
        if(($qObj.STATE -eq "Disconnected") -and ((($Identitys -contains $qObj.USERNAME) -and ($Disconnect -eq $true)) -or (($DiscoAll -ne $false) -and ($qObj.ID -ne 0) -and ($qObj.USERNAME -ne "") -and ($qObj.USERNAME -notlike "svc*")))) {
          $killedUser = rwinsta $qObj.ID /server:$ServerName
          Write-Output $("Terminating session $($qObj.ID) for $($qObj.USERNAME) on $($ServerName)")
        }
      }
    }
    catch {
      Write-Warning "RPC Server unavailable.  Skipping..."
    }
  }
}
export-modulemember -function killsession