<#
  .Synopsis
   Modify user information from Active Directory

  .Description
   Allows user information to be modified within active directory, specifically limited to attributes commonly supported by HelpDesk. 

   The following methods are available:
     Edit-User -Identity tstAcct -Action enable
     Edit-User -Identity tstAcct -Action disable
     Edit-User -Identity tstAcct -Action extension -Value 8888
     Edit-User -Identity tstAcct -Action copygroups -Origin OldUser

  .Parameter Identity
  The username of the user in question

  .Parameter Action
  The action to be performed on the account
  
  .Parameter Value
  The value to be set. Used for some actions, see help.

  .Example
   # enable tstAcct
   Edit-User -Identity tstAcct -Action enable
   -or-
   Edit-User tstAcct enable

  .Example
   # disable tstAcct
   Edit-User -Identity tstAcct -Action disable
   -or-
   Edit-User tstAcct disable

  .Example 
   # Update extension
   Edit-User tstAcct phone 8888
   -or-
   Edit-User -Identity tstAcct -Action phone -Value 8888

  .Example
   # Copy permissions from OldUser to tstAcct
   Edit-User -Identity tstAcct -Action copygroups -Origin OldUser

  .Example
   # Change users expiration to December 31st, 2022
   Edit-User -Identity tstAcct -Action expire -Value 12/31/2022

  .Example
   # Change users title to: System Administrator
   Edit-User -Identity tstAcct -Action title -Value "System Administrator"

#>

Function Roll-Bones {
  $result = "";
  For($i=1;$i -le 5;$i++){
    $currentRoll = Get-Random -Minimum 1 -Maximum 6;
    $result = $result.ToString() + $currentRoll.ToString();
  }
  return $result;
}

Function Generate-Password {
  $Pass = ""
  if(!(Test-Path -path '.\dict.txt')) {
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    Invoke-WebRequest -Uri https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt -OutFile .\dict.txt
  }
  $dictItems = Get-Content .\dict.txt
  $passList = @{}
  ForEach($lineItem in $dictItems) {
    $passList.Add($lineItem.Split("")[0], $lineItem.Split("")[1])
  }
  $valid = $false
  while($valid -eq $false) {
    $num = Roll-Bones
    $Pass = (Get-Culture).TextInfo.ToTitleCase($passList.Get_Item($num)) + (Get-Random -Minimum 101 -Maximum 99999)
    if($Pass.Length -gt 7) {
      $valid = $true
    }
  }
  return $Pass
}

Function Edit-User {
  param (
    [Parameter(Mandatory=$true)][string[]]$Identitys = $null,
    [Parameter(Mandatory=$true)][string]$Action = $null,
    [string]$Value = $null,
    [string]$Origin = $null,
    [switch]$Passforce = $false
  )

  Import-Module ActiveDirectory

  foreach ($Identity in $Identitys) {
    try {
      $User = Get-ADUser -Identity $Identity -Properties *
    } catch {
      Write-Output $("$($Identity): Username not valid.")
      Return
    }

    switch($Action) {
      "copygroups" {
        try {
          $Source = Get-ADUser -Identity $Origin -Properties *
        } catch {
          Write-Output $("Invalid Origin User.")
          Return
        }
        Write-Output $("Copying user groups from $($Source.name) to $($User.name)...")
        Get-ADUser -Identity $Source.samAccountName -Properties memberof | Select-Object -ExpandProperty memberof | Add-ADGroupMember -Members $User.samAccountName -PassThru | Select-Object -Property SamAccountName
      }
      "disable" {
        if($User.Enabled) {
          Write-Output $("FOUND USER:")
          Usercheck $User.samAccountName
          $Response = Read-Host Are you sure you would like to disable? `(y`/n`)
          if($Response -eq "y") {
            Disable-ADAccount -Identity $User.samAccountName
            try {
              Add-ADGroupMember "Disabled Users" $User.samAccountName
            } catch { Write-Output $("Disabled Users Group does not exist... Skipping.") }
            Write-Output $("NEW STATE:")
            Usercheck $User.samAccountName
          } else {
            Write-Output Cancelling...
          }
        } else {
          Write-Output $("User already disabled.")
          Usercheck $User.samAccountName
        }
      }
      "enable" {
        if($User.Enabled) {
          Write-Output $("User already enabled.")
          Usercheck $User.samAccountName
        } else {
          Write-Output $("OLD STATE:")
          Usercheck $User.samAccountName
          Enable-ADAccount -Identity $User.samAccountName
          Write-Output $("NEW STATE:")
          Usercheck $User.samAccountName
        }
      }
      "expire" {
        if(($Value -eq $null) -or ($Value -eq "")) {
          Write-Output $("Expiration Date missing.")
        } else {
          if($Value -eq "never") {
            Write-Output $("Removing expiration for $($User.name)")
            Clear-ADAccountExpiration -Identity $User.samAccountName
          } else {
            if(($User.AccountExpirationDate -eq "") -or ($User.AccountExpirationDate -eq $null)) {
              Write-Output $("NOTE: User is currently not set to expire.")
              $Response = Read-Host Are you sure you would like to change the user to expire? `(y`/n`)
              if($Response -ne "y") {
                Write-Output Cancelling...
		Return
              }
            }
            $realValue = $Value + " 19:00:00"
            Write-Output $("Setting expiration for $($User.name) to $($realValue)")
            Set-ADAccountExpiration -Identity $User.samAccountName -DateTime $realValue
          }
        }
      }
      "password" {
        $newPass = Generate-Password
        Write-Output $("Setting password for $($User.name) `( $($User.samAccountName) `) to $($newPass)...")
        Set-ADAccountPassword -Identity $User.samAccountName -NewPassword (ConvertTo-SecureString -AsPlainText $newPass -Force)
        $tOut = "`|`n"
        $tOut += "`| Password Set to:`n"
        $tOut += "`|`t`t`t`t"
        $tOut += "$($newPass)`n"
        $tOut += "`|`n"
        Write-Output $tOut
        if($Passforce -ne $true) {
          Set-ADUser -Identity $User.samAccountName -ChangePasswordAtLogon $true
        }
      }
      "phone" {
        if(($Value -eq $null) -or ($Value -eq "")) {
          Write-Output $("Extension value missing`, please correct.")
        } else {
          $Response = Read-Host User currently has extension $($User.ipPhone) assigned`, are you sure you want to change? `(y`/n`)
          if($Response -eq "y") {
            Set-ADUser -Identity $User.samAccountName -clear ipphone,telephonenumber
            Set-ADUser -Identity $User.samAccountName -add @{
              ipPhone=$Value;
              telephoneNumber=$Value;
            }
            Usercheck $User.samAccountName
          } else {
            Write-Output Cancelling...
          }
        }
      }
      "title" {
        if(($Value -eq $null) -or ($Value -eq "")) {
          Write-Output $("Title not valid`, please correct.")
        } else {
          Write-Output $("Setting title to $($Value)")
          Set-ADUser -Identity $User.samAccountName -title $Value
          Set-ADUser -Identity $User.samAccountName -description $Value
          Usercheck $User.samAccountName
        }
      }
      "unlock" {
        $LockQuery = Get-ADUser $User.samAccountName -Properties * | Select-Object LockedOut
        if($LockQuery.LockedOut) {
          Write-Output $("Unlocking $($User.samAccountName)...")
          Unlock-ADAccount -Identity $User.samAccountName
        } else {
          Write-Output $("$($User.samAccountName) is not currently locked out.")
        }
      }
      default {
        Write-Output $("Please refer to documentation for proper usage.")
      }
    }
  }
}
export-modulemember -function Edit-User