<#
  .Synopsis
   Display user information from Active Directory

  .Description
   Gathers, formats, and displays information regarding the requests user(s).  This information can be utilized for a quick audit of the users account.
   You may either search by username (or partial), or name (or partial).

  .Example
   #Searching for "jamie"
   Usercheck jamie

  .Example
   #Searching for jam4376
   Usercheck jam4376

#>

Function Usercheck {
  param (
    [Parameter(Mandatory=$true)][string]$UserName = ""
  )

  Import-Module ActiveDirectory
  $Query = '*'+$UserName+'*'
  $UserList = Get-ADUser -Properties * -Filter {(samAccountName -like $Query) -or (DisplayName -like $Query) -or (Name -like $Query) -or (extensionAttribute15 -like $Query)}

  ForEach ($User in $UserList) {
    $msg = ""
    $msg += "`n$($User.DisplayName) - $($User.Title)`n"
    $msg += "=====================================================================================`n"
    $msg += "$($User.DistinguishedName)`n`n"
    $msg += "Username:`t$($User.samAccountName)`n"

    if($User.Enabled) {
      $cDisabled = "False"
    } else { $cDisabled = "True" }

    $msg += "Locked Out:`t$($User.LockedOut)`t`tDisabled:`t$($cDisabled)"

    if(($User.accountExpires -eq 0) -or ($User.accountExpires -eq 9223372036854775807)) {
      $cExp = "Never"
    } else { 
      $cExp = [DateTime]::FromFileTime($User.accountExpires) 
      if($User.accountExpirationDate -ge (Get-Date)) {
        $cExpired = "False"
       } else { $cExpired = "True" }
    }

    if($cExp -ne "Never") {
      $msg += "`nExpired:`t$($cExpired)`t`tExpires:`t$($cExp)`n"
    } else { $msg += "`t`tExpires:`tNever`n" }

    if($User.extensionAttribute15 -ne $null) {
      $msg += "`nAdditional Accounts:`t$($User.extensionAttribute15)`n"
    }

    if(($User.telephoneNumber -eq 99999) -or ($User.telephoneNumber -eq $null)) {
      $Phone = "None"
    } else { $Phone = $User.telephoneNumber }

    $msg += "`nPhone:`t`t$($Phone)`t`tEmail:`t`t$($User.mail)`n`n"
    $msg += "Department:`t$($User.Department)`n"
    $msg += "Description:`t$($User.Description)`n"

    if($User.Manager) {
      $cMan = Get-ADUser $User.Manager -Properties DisplayName
      $cManager = $cMan.DisplayName
    } else {$cManager = "None"}

    $msg += "Manager:`t$($cManager)`n"
    $msg += "`nCreated:`t$($User.Created)`t`tLast Logon:`t$($User.lastLogonDate)`n"
    $pwdAge = 0;
    if(($User.PasswordLastSet -eq "") -or ($User.PasswordLastSet -eq $null)) {
      $pwdAge = "INDEFINITE"
    } else {
      $pwdAge = (New-TimeSpan -Start $User.PasswordLastSet -End (Get-Date)).Days
    }
    if($User.PasswordExpired) {
      $mustChange = "Yes"
    } else {
      $mustChange = "No"
    }
    $msg += "PWD Last Set:`t$($User.PasswordLastSet)`t`tPWD Age:`t$($pwdAge) Days`n"
    $msg += "Must change PWD?`t$($mustChange)`n"
    $msg += "`n=====================================================================================`n`n"

    Write-Output $msg
  }
}
export-modulemember -function Usercheck